from chef import Chef
class Neko_Chef(Chef):

    def make_broiled_dragonfish(self):
        print("Neko-chef broils a dragonfish to perfection. This is incredible!")
    def make_poached_jellyfish(self):
        print("Neko-chef expertly poaches a jellyfish. This is not something you normally order but somehow the chef got it just right.")
    def make_sandwich(self):
        print("Neko-chef makes you a lavish eel sandwich. You can't help but notice the tail hanging out of the end of the bread,")
