from chef import Chef
class Human_Chef(Chef):

    def make_broiled_dragonfish(self):
        print("Thomas broils a dragonfish. A light yet filling meal.")
    def make_poached_jellyfish(self):
        print("Thomas ferments a sirenheart. The flavor is intoxicating.")
    def make_sandwich(self):
        print("Thomas glares at you angrily and yells you to make your own damn sandwich!")

