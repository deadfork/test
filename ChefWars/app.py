#import parent objects
from chef import Chef
from heffalump_chef import Heffalump_Chef
from human_chef import Human_Chef
from neko_chef import Neko_Chef
import random

chef_list = ["Thomas the Human", "Neko the Cat", "Hubert the Heffalump"]

#get input from player to determine who will be cooking and then what they will make
chef_choice = input(print("Welcome to Fishy Business! We have the following chef here today: " + random.choice(chef_list) + "\nWhom would you like to ask for? "))
if chef_choice == "Thomas the Human":
    myChef = Human_Chef()
    print("Very well. Thomas can make the following dishes: Broiled Dragonfish, Fermented Sirenheart, and Poached Jellyfish.\n ")
    selection = input("What would you like to order? ")

elif chef_choice == "Neko the Cat":
    myChef = Neko_Chef()
    print("Very well. Neko can make the following dishes: Broiled Dragonfish, Poached Jellyfish, and a sandwich.\n ")
    selection = input("What would you like to order? ")

elif chef_choice == "Hubert the Heffalump":
    myChef = Heffalump_Chef()
    print("Very well. Hubert is our newest chef and currently offers a single dish, from a forest that prides itself on having 100 acres: Honey Glazed Bear Flank.\n")
    selection = input("What would you like to order? ")

elif chef_choice == "Mahanar":
    print("You don't have the funds to get him to cook for you.")
    selection = "no_selection"

elif chef_choice == "Spoon":
    print("Spoon hears you say its name and waves at you. You go to ask it to cook something for you and it disappears.")
    selection = "no_selection"

else:
    print("Sorry but we don't have anyone like that here. If you want, you can request a sandwich.")

#display output passed on user selection and if input has an issue, catch the error
if selection == "Broiled Dragonfish":
    myChef.make_broiled_dragonfish()
elif selection == "Fermented Sirenheart":
    myChef.make_fermented_sirenheart()
elif selection == "Honey Glazed Bear Flank":
    myChef.make_honey_glazed_bear_flank()
elif selection == "Poached Jellfish()":
    myChef.make_poached_jellyfish()
elif selection == "Sandwich":
    myChef.make_sandwich()
elif selection == "no_selection":
    print("Please try a different option next time.")
else:
    print("I am sorry but we don't offer that at this time.")
